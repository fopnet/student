Aplicação de cadastro de Universidade e Estudantes
==================================================

Como rodar?
==========

* Forma 1: Usando o servidor wildfly em /Applications/wildfly-14.0.1.Final por dentro do eclipse
- url da aplicação: http://localhost:8080/web-app/login
- classe para startup a aplicação usando SpringBoot no servidor WildFly 
projeto web-app :: com.company.SpringBootApplication 

* Forma 2: Usando o servidor por fora do eclipse
> ~/wildfly-14.0.1.Final/bin/standalone.sh 

- Entre no console e faça o deploy manualmente subindo o web-app.war compilado pelo maven

* Forma 3: Usando o Spring Boot Application
projeto web-app :: com.company.App 
- serve para testar algumas injeções de dependências com o codigo de exemplo comentado
- para usá-lo é preciso comentar as anotacoes do com.company.SpringBootApplication para evitar conflito

Iniciando banco de dados Mysql
------------------------------

> verificando se o banco está online
ps aux|grep mysql

> ou vá preferencias do sistema -> mysql -> enable
https://tableplus.io/blog/2018/10/how-to-start-stop-restart-mysql-server.html

> mysql -u students -p
pwd: admin

