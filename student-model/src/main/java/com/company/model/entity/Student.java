package com.company.model.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "students")
@NamedQueries({ @NamedQuery(name = "student.getByAge", query = "Select s from Student s where s.age =:age") })
public class Student implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
//	@Valid
//	@EmbeddedId
	private Integer id;

	@Column()
	@NotNull(message = "O nome é obrigatório")
	private String firstName;

	@Column()
	@NotNull(message = "O sobrenome é obrigatório")
	private String lastName;

	@Column()
	@NotNull(message = "A idade é obrigatória")
	@Min(value = 1, message = "Menor valor é 1")
	@Max(value = 100, message = "Menor valor é 100")
	private Integer age;

	@Version
	@Column()
	@Temporal(TemporalType.TIMESTAMP)
	@NotNull
	private Date lastUpdated;

	@Column()
	@NotNull(message = "O sexo é obrigatório")
	private String gender;

	@NotNull(message = "A universidade é obrigatória")
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "universityId")
	private University university;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

//	@Override
//	public Integer hashCode() {
//		return HashCodeHelper.hashCode(this, "codigo");
//	}
//
//	@Override
//	public boolean equals(Object obj) {
//		return EqualsHelper.isEquals(this, obj, "codigo");
//	}

	public Date getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(Date lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public University getUniversity() {
		return university;
	}

	public void setUniversity(University university) {
		this.university = university;
	}

	public String toString() {
		return this.firstName + " - " + this.age + " years old";
	}

}
