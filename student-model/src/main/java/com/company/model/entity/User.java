package com.company.model.entity;

import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

@Entity
@Table(name = "users")
public class User implements UserDetails {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column()
	@NotNull(message="Digite o nome do usuário")
	private String username;

	@Column()
	@NotNull(message="Digite uma senha")
	@Size(min = 6, message = "Precisa ter no mínimo 6 caracteres")
	private String password;

//	@ManyToOne(fetch = FetchType.EAGER)	
//	@Transient
//	private Collection<SimpleGrantedAuthority> authorities;

	public User() {
//		this.authorities = new ArrayList<SimpleGrantedAuthority>();
	}

	public User(String username2, String pwd) {
		this();
		this.setUsername(username2);
		this.setPassword(pwd);
	}

	//////////////////////////////////////////////////////
	// UserDetails methods
	//////////////////////////////////////////////////////

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public Collection<SimpleGrantedAuthority> getAuthorities() {
//		return authorities;
		return null;
	}

	@Override
	public String getPassword() {
		return password;
	}

	@Override
	public String getUsername() {
		return username;
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

}
