package com.company.model;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.company.model.entity.Student;
import com.company.model.entity.University;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class)
public class StudentTest {

	@Test
	public void testStudentMock() {

		University u = mock(University.class);

		Student s = mock(Student.class);

		when(s.getUniversity()).thenReturn(u);

		assertEquals(s.getUniversity(), u);

	}

}
