package com.company.dao;

import java.lang.reflect.ParameterizedType;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

public abstract class AbstractDao<M, K> implements Dao<M, K> {
	
	@PersistenceContext
	protected final EntityManager em = null;
	
	protected Class<M> persistentClass;

	@SuppressWarnings("unchecked")
	AbstractDao() {
//		this.em = em;
		this.persistentClass = 
				   ((Class<M>) ((ParameterizedType)this.getClass().getGenericSuperclass())
				      .getActualTypeArguments()[0]);

		System.out.println(this.persistentClass);
	}

	public M getById(K key) {
		return this.em.find(this.persistentClass, key);
	}

	@Override
	public M remove(M model) {
		this.em.remove(model);
		return model;
	}

	@Override
	public M insert(M model) {
		this.em.persist(model);

		return model;
	}

	@SuppressWarnings("unchecked")
	public List<M> list() {
		StringBuilder sb = new StringBuilder();
		sb.append("Select e from ");
		sb.append(this.persistentClass.getName());
		sb.append(" e ");

		return this.em.createQuery(sb.toString()).getResultList();
	}

	/////////////////
	// Abstract methods
	/////////////////

}
