package com.company.dao;

import java.util.List;

/**
 * 
 * @author Felipe
 *
 * @param <M> Model Type
 * @param <K> Key Type
 */
public interface Dao<M, K> {

	public M remove(M key);

	public M insert(M model);

	public M getById(Integer key);

	public List<M> list();

}
