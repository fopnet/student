package com.company.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.company.model.entity.Student;

@Repository(value="studentDao")
public interface StudentDao extends JpaRepository<Student, Integer> {
//	public List<Student> getByAge(Integer age);
	
	@Query("select s from Student s JOIN Fetch s.university u order by s.firstName")
	List<Student> findAll();
}
