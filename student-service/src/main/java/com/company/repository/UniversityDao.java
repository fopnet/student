package com.company.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.company.model.entity.University;

@Repository(value="universityDao")
public interface UniversityDao extends JpaRepository<University, Integer> {
	
	@Query("select count(s) from Student s where s.university.id = :id")
	public Integer getNumberOfStudents(@Param("id") Integer id);
}
