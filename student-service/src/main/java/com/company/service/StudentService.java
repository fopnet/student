package com.company.service;

import java.util.List;

import com.company.model.entity.Student;

public interface StudentService {
	
	public void save(Student studentdao);
	
	List<Student> findAll();

	public void remove(Student selectItem);

}
