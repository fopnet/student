package com.company.service;

import java.util.List;

import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.model.entity.Student;
import com.company.repository.StudentDao;

@Service
@Transactional(readOnly = true)
public class StudentServiceImpl implements StudentService {

	@Autowired
	private StudentDao studentDao;

	@Transactional
	public void save(Student model) {

		Student entity = SerializationUtils.clone(model);

//		Student entity = new Student() ;
//		entity.setFirstName(model.getFirstName());
//		entity.setLastName(model.getLastName());
//		entity.setAge(model.getAge());
//		entity.setGender(model.getGender());
//		entity.setUniversity(model.getUniversity());

		studentDao.save(entity);

	}

	@Override
	public List<Student> findAll() {
		List<Student> students = studentDao.findAll();
//		for (Student s: students) {
//			s.getUniversity(); // avoid org.hibernate.LazyInitializationException
//		};
		return students;
	}

	@Override
	public void remove(Student selectItem) {
		studentDao.delete(selectItem);
	}

}
