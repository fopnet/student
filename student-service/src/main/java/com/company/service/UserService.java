package com.company.service;

import org.springframework.security.core.userdetails.UserDetailsService;


public interface UserService extends UserDetailsService {

	void save(String username, String pwd);


}
