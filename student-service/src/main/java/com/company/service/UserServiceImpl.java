package com.company.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.model.entity.User;
import com.company.repository.UserDao;

@Service
@Transactional(readOnly = true)
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao userDao;

	@Autowired
	private BCryptPasswordEncoder encoder;

	@Override
	@Transactional
	public void save(String username, String pwd) {
		User usr = new User(username, encoder.encode(pwd));
		userDao.save(usr);
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User usr = userDao.findByUsername(username);
		return new CustomUserDetails(usr.getUsername(), usr.getPassword(), true, true, true, true,
				usr.getAuthorities());
	}
}
