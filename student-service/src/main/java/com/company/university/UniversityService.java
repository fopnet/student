package com.company.university;

import java.util.List;

import com.company.model.entity.University;

public interface UniversityService {

	public void save(University studentdao);

	List<University> findAll();

	Integer getNumberOfStudents(Integer id);
	
}
