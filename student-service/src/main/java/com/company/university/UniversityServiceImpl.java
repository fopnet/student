package com.company.university;

import java.util.List;


import org.apache.commons.lang3.SerializationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.company.model.entity.University;
import com.company.repository.UniversityDao;

@Service
@Transactional(readOnly = true)
public class UniversityServiceImpl implements UniversityService {

	@Autowired
	private UniversityDao universitytDao;
	
	@Transactional
	public void save(University model) {
		
		University entity = SerializationUtils.clone(model);
//		University entity = new University() ;
//		entity.setName(model.getName());
//		entity.setCountry(model.getCountry());
//		entity.setCity(model.getCity());
//		
		universitytDao.save(entity);
		
	}

	@Override
	public List<University> findAll() {
		return universitytDao.findAll();
	}

	@Override
	public Integer getNumberOfStudents(Integer id) {
		return universitytDao.getNumberOfStudents(id);
	}


}
