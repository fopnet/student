package com.company.dao;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Transactional;

import com.company.service.UserService;
import com.company.service.UserServiceImpl;

//@RunWith(SpringJUnit4ClassRunner.class)
//@ContextConfiguration(classes = {AppConfig.class})

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppConfig.class)
@AutoConfigureTestDatabase(replace = Replace.NONE)
@Transactional
@Rollback(value = true)
public class UserServiceTest {

	// DI
	@Autowired
	private UserService service;

	@Test
	public void testSaveUser() {

		// assert correct type/impl
		assertThat(service, instanceOf(UserServiceImpl.class));

		final String username = "test_user";

		service.save(username, "my-password");

		UserDetails usr = service.loadUserByUsername(username);

		assertNotNull("User must be not null", usr);

		assertEquals(usr.getUsername(), username);

	}
}
