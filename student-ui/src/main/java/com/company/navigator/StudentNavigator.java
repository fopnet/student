package com.company.navigator;

import com.company.ui.commons.MainUi;
import com.google.common.base.Strings;
import com.vaadin.navigator.Navigator;
import com.vaadin.ui.UI;

public class StudentNavigator extends Navigator {

	public StudentNavigator(MainUi ui, com.vaadin.ui.Panel container) {
		super(ui, container);
	}

	private static StudentNavigator getNavigator() {
		UI ui = UI.getCurrent();
		Navigator navigator = ui.getNavigator();
		return (StudentNavigator) navigator;
	}

	public static void navigate(String path) {
		try {
			StudentNavigator.getNavigator().navigateTo(path);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void navigateTo(String viewName) {
		super.navigateTo(Strings.nullToEmpty(viewName));
	}
}
