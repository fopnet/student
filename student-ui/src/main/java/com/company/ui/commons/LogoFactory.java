package com.company.ui.commons;

import com.vaadin.server.ThemeResource;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.Embedded;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design.ComponentFactory;
import com.vaadin.ui.declarative.DesignContext;

@org.springframework.stereotype.Component
public class LogoFactory implements ComponentFactory {

	private class LogoLayout extends VerticalLayout {
		private Embedded logo;

		public LogoLayout init() {
			logo = new Embedded();
			logo.setSource(new ThemeResource("../../images/logo.png"));
			logo.setWidth("219px");
			logo.setHeight("230px");
			return this;
		}
		
		public LogoLayout layout() {
			this.addComponent(logo);
			setComponentAlignment(logo, Alignment.MIDDLE_CENTER);
			return this;
		}
	}

	@Override
	public Component createComponent(String fullyQualifiedClassName, DesignContext context) {
		return new LogoLayout().init().layout();
	}

}
