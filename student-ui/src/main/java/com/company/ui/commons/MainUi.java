package com.company.ui.commons;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import com.company.navigator.StudentNavigator;
import com.company.ui.students.StudentLayoutFactory;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Panel;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

@SpringUI(path = MainUi.NAME)
@Title("S t u d e n t s")
@Theme("valo")
public class MainUi extends UI {

	private static final long serialVersionUID = -1818847013843663227L;

	public static final String NAME = "/ui";

	private Panel changePanel = new Panel();

	@Autowired
	private LogoFactory logoFactory;

	@Autowired
	private MenuFactory menuFactory;

	@Autowired
	private ApplicationContext appContext;

	@Autowired
	private SpringViewProvider viewProvider;

	@Override
	protected void init(VaadinRequest request) {

		this.changePanel.setHeight("100%");

		VerticalLayout root = new VerticalLayout();

		root.setSizeFull();
		root.setMargin(true);

		Panel contentPanel = new Panel();
		contentPanel.setWidth("75%");
		contentPanel.setHeight("100%");

		Panel logoPanel = new Panel();
		logoPanel.setWidth("75%");
		logoPanel.setHeightUndefined();

		HorizontalLayout hr = new HorizontalLayout();
		hr.setSizeFull();
		hr.setMargin(true);

		Component logo = this.logoFactory.createComponent(null, null);
		Component menu = this.menuFactory.createComponent(null, null);

		hr.addComponent(menu);
		hr.addComponent(this.changePanel);

		hr.setComponentAlignment(this.changePanel, Alignment.TOP_CENTER);
		hr.setComponentAlignment(menu, Alignment.TOP_CENTER);

		hr.setExpandRatio(menu, 1f);
		hr.setExpandRatio(this.changePanel, 2f);

		logoPanel.setContent(logo);
		contentPanel.setContent(hr);

		root.addComponent(logoPanel);
		root.addComponent(contentPanel);
		root.setComponentAlignment(contentPanel, Alignment.MIDDLE_CENTER);
		root.setComponentAlignment(logoPanel, Alignment.TOP_CENTER);
		root.setExpandRatio(contentPanel, 1);

		initNavigator();

		setContent(root);

	}

	private void initNavigator() {
		StudentNavigator navigator = new StudentNavigator(this, this.changePanel);
		this.appContext.getAutowireCapableBeanFactory().autowireBean(navigator);
		navigator.addProvider(this.viewProvider);
		navigator.navigateTo(StudentLayoutFactory.name);

	}

}
