package com.company.ui.commons;

import static com.company.util.StringUtils.MENU_STUDENTS;
import static com.company.util.StringUtils.MENU_UNIVERSITY;

import org.springframework.security.core.context.SecurityContextHolder;

import com.company.navigator.StudentNavigator;
import com.google.common.base.Strings;
import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.Tree;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design.ComponentFactory;
import com.vaadin.ui.declarative.DesignContext;

@org.springframework.stereotype.Component
public class MenuFactory implements ComponentFactory {

	private class MenuLayout extends VerticalLayout implements Property.ValueChangeListener {
		private Tree mainMenu;

		public MenuLayout init() {
			mainMenu = new Tree();
			mainMenu.setWidth("219px");
			mainMenu.setHeight("230px");
			mainMenu.addValueChangeListener(this);
			return this;
		}
		
		public MenuLayout layout() {
			setWidth("100%");
			setHeightUndefined();
			
			mainMenu.addItem(MENU_STUDENTS);
			mainMenu.addItem(MENU_UNIVERSITY);
			mainMenu.addItem("LOGOUT");
			
			mainMenu.expandItem(MENU_STUDENTS);
			mainMenu.expandItem(MENU_UNIVERSITY);
			mainMenu.expandItem("LOGOUT");
			
			mainMenu.addItem("Add Student");
			mainMenu.addItem("Remove Student");
			mainMenu.setChildrenAllowed("Add Student", false);
			mainMenu.setChildrenAllowed("Remove Student", false);
			
			mainMenu.addItem("Logout");
			mainMenu.setChildrenAllowed("Logout", false);
			mainMenu.setParent("Logout", "LOGOUT");
			
			mainMenu.setParent("Add Student", MENU_STUDENTS);
			mainMenu.setParent("Remove Student", MENU_STUDENTS);
			
			mainMenu.addItem("Operations");
			mainMenu.setChildrenAllowed("Operations", false);
			mainMenu.setParent("Operations", MENU_UNIVERSITY);
			
			addComponent(mainMenu);
			return this;
		}

		@Override
		public void valueChange(ValueChangeEvent event) {
			String path = (String) event.getProperty().getValue().toString();
			
			if (Strings.isNullOrEmpty(path)) return;
			
//			System.out.println(path);
			
			if (path.equalsIgnoreCase("logout")) {
				SecurityContextHolder.clearContext();
				UI.getCurrent().getPage().setLocation("/web-app/login");
			}
			path  = path.toLowerCase().replaceAll("\\s+", "");
			
			StudentNavigator.navigate(path);
			
		}
	}

	@Override
	public Component createComponent(String fullyQualifiedClassName, DesignContext context) {
		return new MenuLayout().init().layout();
	}

}
