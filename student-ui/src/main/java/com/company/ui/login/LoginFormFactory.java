package com.company.ui.login;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;

import com.company.model.entity.User;
import com.company.util.NotificationMessages;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.event.ShortcutAction;
import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@org.springframework.stereotype.Component
public class LoginFormFactory {

	@Autowired
	private DaoAuthenticationProvider provider;

	private class LoginForm {

		private VerticalLayout layout;
		private Panel panel;
		@PropertyId("username")
		private TextField usernameField;
		@PropertyId("password")
		private PasswordField pwdField;
		private Button loginButton;
		private Button signupButton;
		private User user;
		
		private BeanFieldGroup<User> fieldGroup = new BeanFieldGroup<User>(User.class);

		private LoginForm init() {

			layout = new VerticalLayout();
			layout.setMargin(true);
			layout.setHeight("100%");

			panel = new Panel("Login");
			panel.setSizeUndefined();

			loginButton = new Button("Login");
			loginButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);

			signupButton = new Button("Cadastre-se");
			signupButton.setStyleName(ValoTheme.BUTTON_PRIMARY);

			usernameField = new TextField("Usuário");
			usernameField.setNullRepresentation("");
			pwdField = new PasswordField("Senha");
			pwdField.setNullRepresentation("");
			
			user = new User();

			return this;

		}

		public LoginForm bind() {
			this.fieldGroup.bindMemberFields(this);
			this.fieldGroup.setItemDataSource(this.user);
			return this;
		}
		
		public Component layout() {
			layout.addComponent(panel);
			layout.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

			FormLayout form = new FormLayout();
			form.setSizeUndefined();
			form.setMargin(true);
			form.setHeightUndefined();
			form.addComponent(usernameField);
			form.addComponent(pwdField);

			HorizontalLayout hr = new HorizontalLayout(loginButton, signupButton);
			hr.setSpacing(true);
			form.addComponent(hr);

			panel.setContent(form);

			usernameField.focus();
			
			pwdField.setImmediate(true);
			pwdField.addShortcutListener(new ShortcutListener("EnterKeyPress", ShortcutAction.KeyCode.ENTER, null) {

				@Override
				public void handleAction(Object sender, Object target) {
					login();
				}
			});

			loginButton.addClickListener(event -> login());

			signupButton.addClickListener(new Button.ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					UI.getCurrent().getPage().setLocation("/web-app/signup");
				}

			});

			return layout;
		}

		public void login() {
			try {
				Authentication auth = new UsernamePasswordAuthenticationToken(usernameField.getValue(),
						pwdField.getValue());

				Authentication authenticated = provider.authenticate(auth);

				SecurityContextHolder.getContext().setAuthentication(authenticated);

				UI.getCurrent().getPage().setLocation("/web-app/ui");

			} catch (AuthenticationException e) {
				Notification.show(NotificationMessages.LOGIN_FAIL.name(),
						NotificationMessages.LOGIN_FAIL.getDescription(), Type.ERROR_MESSAGE);
			}

			usernameField.clear();
			pwdField.clear();
		}
	}

	public Component createComponent() {
		return new LoginForm().init().bind().layout();
	}

}
