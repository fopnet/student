package com.company.ui.login;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.User;
import com.company.service.UserService;
import com.company.util.NotificationMessages;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Component;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@org.springframework.stereotype.Component
public class SignUpFormFactory {

	@Autowired
	private UserService service;

	private class SignUpForm {

		private VerticalLayout root;
		private Panel panel;
		@PropertyId("username")
		private TextField usernameField;
		@PropertyId("password")
		private PasswordField passwordField;
		private PasswordField confirmField;

		private BeanFieldGroup<User> fieldGroup = new BeanFieldGroup<User>(User.class);
		private User user;

		private Button saveButton;

		private SignUpForm init() {

			root = new VerticalLayout();
			root.setMargin(true);
			root.setHeight("100%");

			panel = new Panel("SingUp");
			panel.setSizeUndefined();

			saveButton = new Button("Save");
			saveButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);

			usernameField = new TextField("Usuário");
			usernameField.setNullRepresentation("");
			passwordField = new PasswordField("Senha");
			passwordField.setNullRepresentation("");
			confirmField = new PasswordField("Confirme a senha");
			confirmField.setNullRepresentation("");

			saveButton.addClickListener(new Button.ClickListener() {

				@Override
				public void buttonClick(ClickEvent event) {
					if (!passwordField.getValue().equals(confirmField.getValue())) {
						Notification.show("Error", "Senha não confere", Type.ERROR_MESSAGE);
						return;
					}

					service.save(usernameField.getValue(), passwordField.getValue());

					Notification.show(NotificationMessages.USER_SAVED_SUCCESS.name(),
							NotificationMessages.USER_SAVED_SUCCESS.getDescription(), Type.ERROR_MESSAGE);

					UI.getCurrent().getPage().setLocation("/web-app/login");
				}

			});

			user = new User();
			
			return this;
		}

		public SignUpForm bind() {
			this.fieldGroup.bindMemberFields(this);
			this.fieldGroup.setItemDataSource(this.user);
			return this;
		}

		private Component layout() {
			root.addComponent(panel);
			root.setComponentAlignment(panel, Alignment.MIDDLE_CENTER);

			FormLayout form = new FormLayout();
			form.addComponent(usernameField);
			form.addComponent(passwordField);
			form.addComponent(confirmField);

			form.addComponent(saveButton);
			form.setSizeUndefined();
			form.setMargin(true);

			panel.setContent(form);

			return root;

		}
	}

	public Component createComponent() {
		return new SignUpForm().init().bind().layout();
	}

}
