package com.company.ui.students;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.Student;
import com.company.service.StudentService;
import com.company.university.UniversityService;
import com.company.util.Gender;
import com.company.util.NotificationMessages;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.data.fieldgroup.PropertyId;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.ComboBox;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@org.springframework.stereotype.Component
public class AddStudentLayoutFactory {
	
	@Autowired
	private StudentService service;
	
	@Autowired
	private UniversityService universityService;

	private class AddStudentLayout extends VerticalLayout implements ClickListener {
		
		@PropertyId("firstName")
		private TextField firstNameTextField;
		private TextField lastName;
		private TextField age;
		private ComboBox gender;
		private ComboBox university;
		private Button saveButton;
		private Button clearButton;

		private BeanFieldGroup<Student> fieldGroup = new BeanFieldGroup<Student>(Student.class);
		private Student student;
		private StudentSavedListener savedListener;

		public AddStudentLayout(StudentSavedListener listener) {
			this.savedListener = listener;
			this.student = new Student();
		}

		public AddStudentLayout init() {
			// TODO change captions to Enum
			firstNameTextField = new TextField("First name");
			lastName = new TextField("Last name");
			age = new TextField("Age");
			gender = new ComboBox("Gender");
			gender.addItem(Gender.FEMALE.toString());
			gender.addItem(Gender.MALE.toString());
			
			university = new ComboBox("University");
			
			saveButton = new Button("Salvar");
			saveButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);
			saveButton.addClickListener(this);

			clearButton = new Button("Limpar");
			clearButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
			clearButton.addClickListener(this);
			
			
			firstNameTextField.setNullRepresentation("");
			lastName.setNullRepresentation("");
			age.setNullRepresentation("");
			
			
			return this;
		}
		
		public AddStudentLayout bind() {
			this.fieldGroup.bindMemberFields(this);
			this.fieldGroup.setItemDataSource(this.student);
			return this;
		}
		
		public AddStudentLayout load() {
			
			university.addItems(universityService.findAll());
			
			return this;
		}

		public Component layout() {
			
			setMargin(true);
			
			GridLayout grid = new GridLayout(2, 4);
			grid.setSizeUndefined();
			grid.setSpacing(true);
			
			grid.addComponent(this.firstNameTextField, 0, 0);
			grid.addComponent(this.lastName, 1, 0);
			
			grid.addComponent(age, 0, 1);
			grid.addComponent(gender, 1, 1);
			grid.addComponent(university, 0, 2, 1, 2);
			
			
			HorizontalLayout hr = new HorizontalLayout(saveButton, clearButton);
			hr.setMargin(true);
			hr.setSpacing(true);
			grid.addComponent(hr, 0, 3);
			
			return grid;
		}

		@Override
		public void buttonClick(ClickEvent event) {
			if (event.getSource() == this.saveButton) {
				save();
			} else {
				clear();
			}
			
		}

		private void save() {
			try {
				// try map the model value to view components
				this.fieldGroup.commit();
				
				 service.save(this.student);
			} catch (CommitException e) {
				Notification.show(
						NotificationMessages.VALIDATION_ERROR.toString(), 
						NotificationMessages.VALIDATION_ERROR.getDescription(),
						Type.ERROR_MESSAGE);
				return;
			}
			
			this.clear();
			this.savedListener.studentSaved();
			
			Notification.show(
					NotificationMessages.STUDENT_SAVED_SUCCESS.toString(), 
					NotificationMessages.STUDENT_SAVED_SUCCESS.getDescription(),
					Type.HUMANIZED_MESSAGE);
			
			
		}

		private void clear() {
			this.firstNameTextField.setValue(null);
			this.lastName.setValue(null);
			this.age.setValue(null);
			this.gender.setValue(null);
			this.university.setValue(null);
			
		}
		
	}

	public Component createComponent(StudentSavedListener listener) {
		return new AddStudentLayout(listener)
				.init()
				.bind()
				.load()
				.layout();
	}

}
