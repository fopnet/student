package com.company.ui.students;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.Student;
import com.company.service.StudentService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;

@org.springframework.stereotype.Component
public class ListStudentLayoutFactory /* implements ComponentFactory */{

	@Autowired
	private StudentService service;
	
	private List<Student> students;
	
	private BeanItemContainer<Student> container;

	private class ListStudentLayout extends VerticalLayout implements ClickListener {
		

		private Grid table;
		private Button button;

		public ListStudentLayout init() {
			// TODO change captions to Enum
			setMargin(true);

			container = new BeanItemContainer<Student>(Student.class, students);

			table = new Grid(container);
			table.setColumnOrder("firstName", "lastName", "age", "gender");
			table.removeColumn("id");
			table.removeColumn("university");
			table.setImmediate(true);
			table.setHeightMode(HeightMode.UNDEFINED); 
			
			button = new Button("Buscar");
			button.setStyleName(ValoTheme.BUTTON_PRIMARY);
			button.addClickListener(this);
			
			return this;
		}

		public Component layout() {

			setMargin(true);

			addComponent(this.table);
			
			HorizontalLayout hr = new HorizontalLayout(button);
			hr.setMargin(true);
			hr.setSpacing(true);

			addComponent(hr);

			return this;
		}

		@Override
		public void buttonClick(ClickEvent event) {
			refreshTable();

		}

		private ListStudentLayout load() {
			students = service.findAll();
			return this;
		}

	}

	public Component createComponent() {
		return new ListStudentLayout().init().load().layout();
	}

	public void refreshTable() {
		students = service.findAll();
		container.removeAllItems();
		container.addAll(students);
	}

	
}
