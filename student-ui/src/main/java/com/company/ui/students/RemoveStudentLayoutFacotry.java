package com.company.ui.students;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.Student;
import com.company.service.StudentService;
import com.company.ui.commons.MainUi;
import com.company.util.NotificationMessages;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Grid;
import com.vaadin.ui.Grid.MultiSelectionModel;
import com.vaadin.ui.Grid.SelectionMode;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@SpringView(name = RemoveStudentLayoutFacotry.NAME, ui = MainUi.class)
public class RemoveStudentLayoutFacotry extends VerticalLayout implements View, Button.ClickListener {

	public static final String NAME = "removestudent";

	private Grid table;
	private Button removeButton;
	private List<Student> students;

	@Autowired
	StudentService service;

	private void addLayout() {
		removeButton = new Button("Remove");
		setMargin(true);
		BeanItemContainer<Student> container = new BeanItemContainer<Student>(Student.class, students);

		table = new Grid(container);

		table.setColumnOrder("firstName", "lastName", "age", "gender");
		table.removeColumn("id");
		table.setImmediate(true);
		table.setHeightUndefined();
		table.setHeightMode(HeightMode.ROW);
		table.setHeightByRows(students.size() <= 5 ? students.size() : 5);
		table.setSelectionMode(SelectionMode.MULTI);

		removeButton.addClickListener(this);
		removeButton.setStyleName(ValoTheme.BUTTON_FRIENDLY);

		addComponent(table);
		addComponent(removeButton);

	}

	private void loadStudents() {
		students = service.findAll();

	}

	@Override
	public void buttonClick(ClickEvent event) {

		MultiSelectionModel selection = (MultiSelectionModel) table.getSelectionModel();
		for (Object selectItem : selection.getSelectedRows()) {
			table.getContainerDataSource().removeItem(selectItem);
			service.remove((Student) selectItem);
		}
		loadStudents();

		Notification.show(NotificationMessages.STUDENT_REMOVE_SUCCESS.name(),
				NotificationMessages.STUDENT_REMOVE_SUCCESS.getDescription(), Type.HUMANIZED_MESSAGE);

		table.getSelectionModel().reset();
	}

	@Override
	public void enter(ViewChangeEvent event) {
		if (table != null)
			return;

		loadStudents();
		addLayout();

	}

}
