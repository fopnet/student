package com.company.ui.students;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.ui.commons.MainUi;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = StudentLayoutFactory.name, ui = MainUi.class)
public class StudentLayoutFactory extends VerticalLayout implements View, StudentSavedListener {

	public static final String name = "addstudent";
	
	@Autowired
	private AddStudentLayoutFactory addStudentFactory;

	@Autowired
	private ListStudentLayoutFactory listStudentFactory;
	
	private TabSheet tabSheet;

	public void enter(ViewChangeEvent event) {
		removeAllComponents();
		addLayout();

	}

	private void addLayout() {

		setMargin(true);
		tabSheet = new TabSheet();
		tabSheet.setWidth("100%");

		Component addTab = this.addStudentFactory.createComponent(this);
		Component showTab = this.listStudentFactory.createComponent(); //new Label("Second tab");

		tabSheet.addTab(addTab, "ADD_STUDENTS");
		tabSheet.addTab(showTab, "SHOW_STUDENTS");
		
		addComponent(tabSheet);

	}

	@Override
	public void studentSaved() {
		this.listStudentFactory.refreshTable();
		
	}

}
