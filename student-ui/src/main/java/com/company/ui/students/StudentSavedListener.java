package com.company.ui.students;

public interface StudentSavedListener {

	public void studentSaved();
}
