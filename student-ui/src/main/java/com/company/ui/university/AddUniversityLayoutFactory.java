package com.company.ui.university;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.University;
import com.company.university.UniversityService;
import com.company.util.NotificationMessages;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@org.springframework.stereotype.Component
public class AddUniversityLayoutFactory {

	@Autowired
	private UniversityService service;

	private class AddUniversityLayout extends VerticalLayout implements Button.ClickListener {

		private TextField name;
		private TextField country;
		private TextField city;
		private Button saveButton;

		private BeanFieldGroup<University> fieldGroup;
		private University university;
		private UniversitySavedListener universitySavedListener;

		public AddUniversityLayout(UniversitySavedListener universitySavedListener) {
			this.universitySavedListener = universitySavedListener;
			this.university = new University();
		}

		@Override
		public void buttonClick(ClickEvent event) {
			if (event.getSource() == this.saveButton) {
				save();
			} else {
				clear();
			}
		}

		private void save() {
			try {
				// try map the model value to view components
				this.fieldGroup.commit();

				service.save(this.university);

			} catch (CommitException e) {
				Notification.show(NotificationMessages.VALIDATION_ERROR.toString(),
						NotificationMessages.VALIDATION_ERROR.getDescription(), Type.ERROR_MESSAGE);
				return;
			}

			this.clear();
			this.universitySavedListener.universitySaved();

			Notification.show(NotificationMessages.UNIVERSITY_SAVED_SUCCESS.toString(),
					NotificationMessages.UNIVERSITY_SAVED_SUCCESS.getDescription(), Type.HUMANIZED_MESSAGE);

		}

		private void clear() {

			this.name.setValue(null);
			this.country.setValue(null);
			this.city.setValue(null);

		}

		public AddUniversityLayout init() {
			name = new TextField("Name");
			country = new TextField("Country");
			city = new TextField("City");

			name.setNullRepresentation("");
			country.setNullRepresentation("");
			country.setValue("Brasil");
			city.setNullRepresentation("");

			saveButton = new Button("Save");
			saveButton.setStyleName(ValoTheme.BUTTON_PRIMARY);
			saveButton.addClickListener(this);

			return this;
		}

		private AddUniversityLayout bind() {
			this.fieldGroup = new BeanFieldGroup<University>(University.class);
			this.fieldGroup.bindMemberFields(this);
			this.fieldGroup.setItemDataSource(this.university);
			return this;
		}

		public Component layout() {
			setMargin(true);

			GridLayout grid = new GridLayout(1, 4);
			grid.setHeightUndefined();
			grid.setSpacing(true);

			grid.addComponent(name, 0, 0);
			grid.addComponent(country, 0, 1);
			grid.addComponent(city, 0, 2);
			grid.addComponent(saveButton, 0, 3);

			return grid;
		}

	}

	public Component createComponent(UniversitySavedListener universitySavedListener) {
		return new AddUniversityLayout(universitySavedListener).init().bind().layout();
	}
}
