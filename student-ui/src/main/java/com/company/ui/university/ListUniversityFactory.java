package com.company.ui.university;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.University;
import com.company.university.UniversityService;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.shared.ui.grid.HeightMode;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.Grid;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.ValoTheme;

@org.springframework.stereotype.Component
public class ListUniversityFactory  /* implements ComponentFactory */{

	@Autowired
	private UniversityService service;
	
	private List<University> universities;
	
	private BeanItemContainer<University> container;

	private class ListUniversityLayout extends VerticalLayout implements ClickListener {
		

		private Grid table;
		private Button button;

		public ListUniversityLayout init() {
			// TODO change captions to Enum
			setMargin(true);

			container = new BeanItemContainer<University>(University.class, universities);

			table = new Grid(container);
			table.setColumnOrder("name", "country", "city");
			table.removeColumn("id");
			table.setImmediate(true);
			table.setHeightMode(HeightMode.ROW); 
			table.setHeightUndefined();
			
			button = new Button("Buscar");
			button.setStyleName(ValoTheme.BUTTON_PRIMARY);
			button.addClickListener(this);
			
			return this;
		}

		public Component layout() {

			setMargin(true);

			addComponent(this.table);
			
			HorizontalLayout hr = new HorizontalLayout(button);
			hr.setMargin(true);
			hr.setSpacing(true);

			addComponent(hr);

			return this;
		}

		@Override
		public void buttonClick(ClickEvent event) {
			refreshTable();

		}

		private ListUniversityLayout load() {
			universities = service.findAll();
			return this;
		}

	}

	public Component createComponent() {
		return new ListUniversityLayout().init().load().layout();
	}

	public void refreshTable() {
		universities = service.findAll();
		container.removeAllItems();
		container.addAll(universities);
	}

	
}
