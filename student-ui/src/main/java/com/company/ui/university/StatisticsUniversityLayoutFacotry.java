package com.company.ui.university;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.model.entity.University;
import com.company.university.UniversityService;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.declarative.Design.ComponentFactory;
import com.vaadin.ui.declarative.DesignContext;

@org.springframework.stereotype.Component
public class StatisticsUniversityLayoutFacotry implements ComponentFactory {

	private List<University> universities;

	@Autowired
	private UniversityService service;
	
	private StatisticsUniversityLayout statsLayout;

	private class StatisticsUniversityLayout extends VerticalLayout {

		private StatisticsUniversityLayout load() {
			universities = service.findAll();
			return this;
		}

		private StatisticsUniversityLayout layout() {
			setMargin(true);

			for (University u : universities) {
				int numberOfStudents = service.getNumberOfStudents(u.getId());
				Label label = new Label("<p><b>" + u.getName() + "</b>" + " - " + numberOfStudents + " </p>",
						ContentMode.HTML);
				
				addComponent(label);
			}
			
			return this;
		}
		
		public void refresh() {
			this.load().layout();
		}

	}

	@Override
	public Component createComponent(String fullyQualifiedClassName, DesignContext context) {
		statsLayout = new StatisticsUniversityLayout();
		statsLayout.refresh();
		return statsLayout;
	}
	
	public void refresh() {
		if (this.statsLayout == null) return;
		
		this.statsLayout.refresh();
	}

}
