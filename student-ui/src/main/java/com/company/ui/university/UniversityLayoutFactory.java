package com.company.ui.university;

import org.springframework.beans.factory.annotation.Autowired;

import com.company.ui.commons.MainUi;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.ui.Component;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.VerticalLayout;

@SpringView(name = UniversityLayoutFactory.name, ui = MainUi.class)
public class UniversityLayoutFactory extends VerticalLayout implements View, UniversitySavedListener {

	public static final String name = "operations";
	
	@Autowired
	private AddUniversityLayoutFactory addUniversityFactory;
	
	@Autowired
	private ListUniversityFactory listUniversityFactory;
	
	@Autowired
	private StatisticsUniversityLayoutFacotry statsFactory;

	private TabSheet tabSheet;

	public void enter(ViewChangeEvent event) {
		removeAllComponents();
		addLayout();

	}

	private void addLayout() {

		setMargin(true);
		tabSheet = new TabSheet();
		tabSheet.setWidth("100%");

		Component addTab = this.addUniversityFactory.createComponent(this);
		Component showTab =  this.listUniversityFactory.createComponent();
		Component stats =  this.statsFactory.createComponent(null, null);

		tabSheet.addTab(addTab, "ADD UNIVERSITY");
		tabSheet.addTab(showTab, "SHOW ALL");
		tabSheet.addTab(stats, "STATISTICS");
		
		addComponent(tabSheet);

	}

	@Override
	public void universitySaved() {
		this.listUniversityFactory.refreshTable();
		this.statsFactory.refresh();
	
	}


}
