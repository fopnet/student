package com.company.util;

public enum Gender {
	MALE("MASCULINO"), FEMALE("FEMININO");

	private String id;

	private Gender(String id) {
		this.id = id;
	}
	
	public String toString() {
		return this.id;
	}

}
