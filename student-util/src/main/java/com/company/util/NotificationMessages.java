package com.company.util;

public enum NotificationMessages {

	VALIDATION_ERROR("ERROR", "Nem todos os campos foram preenchidos!"),
	
	STUDENT_SAVED_SUCCESS("SUCCESS", "Estudante gravado com sucesso!"),
	STUDENT_REMOVE_SUCCESS("SUCCESS", "Estudante removido com sucesso!"),
	
	LOGIN_FAIL("FALHA", "Falha no autenticação do usuário "),
	USER_SAVED_SUCCESS("SUCCESS", "Usuário criado com sucesso. Favor efetuar o login"),

	UNIVERSITY_SAVED_SUCCESS("SUCCESS", "Universidade gravada com sucesso!"),
	;

	private String description;
	private String id;

	private NotificationMessages(String id, String description) {
		this.id = id;
		this.description = description;
	}
	
	public String toString() {
		return this.id;
	}
	
	public String getDescription() {
		return this.description;
	}

}
