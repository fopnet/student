package com.company.util;

public enum StringUtils {

	MENU_STUDENTS("STUDENTS"),
	MENU_UNIVERSITY("UNIVERSITY");
	
	private String id;

	private StringUtils(String st) {
		this.id = st;
	}
	
	public String getId() {
		return this.id;
	}
	
	public String toString() {
		return this.id;
	}
}
