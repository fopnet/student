package com.company;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;

import com.company.service.UserService;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
	private UserService userDetailService;
	
	@Autowired
	public void configGlobal(AuthenticationManagerBuilder builder) throws Exception {
		final DaoAuthenticationProvider provider = createDaoAuthenticationProvider();
		
		builder
		.userDetailsService(userDetailService)
		.and()
		.authenticationProvider(provider);
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
		.csrf()
		.disable()
		.exceptionHandling()
		.authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login"))
		.and()
		.authorizeRequests()
		.antMatchers("/VAADIN/**", "/PUSH/**", "/UIDL/**","/login", "/login/**","/signup","/logout","/vaadinservlet/**")
		.permitAll()
		.antMatchers("/ui","/ui/**")
		.fullyAuthenticated()
//		.and().formLogin().loginPage("/login");	
		;
	}
	
	@Bean
	public DaoAuthenticationProvider createDaoAuthenticationProvider() {
		DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
		provider.setUserDetailsService(userDetailService);
		provider.setPasswordEncoder(getPasswordEncoder());
		return provider;
	}

	@Bean
	public BCryptPasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	
}
